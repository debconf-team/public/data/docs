.. _network-requirements:

Network Requirements for DebConf
================================

This page exists to document the requirements for a DebConf network to an
outsider.

We'd suggest that you use this as a starting-point to write up your specific
event needs (name rooms, dates, etc.) and share that with the venue network
team.

Definitions
-----------

Parts of the event
^^^^^^^^^^^^^^^^^^

.. glossary::

    The Camp
        The informal "camp" that precedes :term:`The Conference` held on
        FIXME through FIXME through.
        Expected attendees: ~100, with the earliest arriving 2 days
        (FIXME) before the start of the camp, to prepare :term:`The
        Camp` and :term:`The Conference` for the arrival of the other
        attendees.

    The Conference
        The formal conference held FIXME through FIXME.
        Expected attendees: ~250 (FIXME), with the latest attendees
        departing on the day after the end of the conference.

Teams
^^^^^

.. glossary::

    The Front Desk Team
        The set of conference organisers that provide attendee-facing
        camp/conference administration services.
        Contact Person is FIXME.

    The Infrastructure Team
        The set of conference organisers responsible for arranging for
        the resources necessary to satisfy the network and computing
        requirements of :term:`The Conference`.
        Contact Person is FIXME.

    The Video Team
        The set of conference organisers that operate :term:`The Video
        Infrastructure`.
        Contact Person is FIXME.

Venues
^^^^^^

.. glossary::

    The Hack Labs
        Rooms dedicated to collaborative project work.
        We are planning to use FIXME as hack labs. Some of these (FIXME)
        will be open 24/7.

    The Videoed Talk Rooms
        The (usually) 3 lecture theatres that the main (video recorded)
        conference events are held in.
        We are planning to use FIXME as the talk rooms.

    The BoF Rooms
        Meeting rooms used by conference attendees during :term:`The
        Conference`.
        We are planning to use FIXME.

    The Front Desk
        The registration desk and attendee assistance desk for the
        conference, run by :term:`The Front Desk Team`.

    The NOC
        A room that :term:`The Infrastructure Team` uses as a work area
        to set up equipment.
        We are planning to use FIXME.

    The Video Team Room
        :term:`The NOC` equivalent for :term:`The Video Team`.
        Can be co-located with :term:`The NOC`, given sufficient space.
        Needs access to :term:`The Video Network`.

    The Server Room
        A room that :term:`The Infrastructure Team` has access to
        (ideally 24/7), hosting conference server infrastructure.
        This can be a traditional server room or simply a room corner of
        :term:`The NOC` / :term:`The Video Team Room`, given sufficient
        network connectivity, power, and cooling.
        Equipment can also be split across physical rooms.

Infrastructure
^^^^^^^^^^^^^^

.. glossary::

    The Internet
        The outside Internet.
        Attendees need no access to the venue's Intranet.

    The DHCP & DNS Server
        A machine in :term:`The Server Room` that provides DHCP and DNS
        service for the conference's networks.

    The Local Mirror Server
        A machine in :term:`The Server Room` that provides a local copy
        of the full Debian archive, should we elect to deploy one.
        The next closest full mirror is FIXME, if we don't deploy a
        local mirror, this will be getting a lot of traffic from our
        attendees.

    The Local Video Storage Server
        A machine in :term:`The Server Room` that provides sufficient
        storage space to store the entire conference's recorded HD
        video.
        This HD video is transferred to the :term:`The Local Video
        Storage Server` from :term:`The Videography Equipment` in
        :term:`The Videoed Talk Rooms` when talks aren't happening.

    The Video Distribution Network
        A network of video relays, hosted around the world (provided by
        :term:`The Video Team`).
        Remote attendees watch live streams of the conference through
        this distribution network.
        :term:`The Video Distribution Network` receives a feed from
        :term:`The Video Streaming Backend Server`.

    The Video Streaming Backend Server
        A machine in :term:`The Server Room` that receives live video
        streams from :term:`The Videography Equipment` in :term:`The
        Videoed Talk Rooms`, transcodes to several formats and relays to
        :term:`The Local Video Streaming Frontend Server` for local
        attendees and to :term:`The Video Distribution Network` for
        remote attendees.

    The Local Video Streaming Frontend Server
        A machine in :term:`The Server Room` that provides local
        attendees with live video streams of the presentations underway
        in :term:`The Videoed Talk Rooms`.
        A mirror of :term:`The Video Streaming Backend Server`'s feeds.

    The Video Transcoding Server
        One or more machines in :term:`The Server Room` that transcodes
        recorded HD video for publication to :term:`The Public Video
        Archive`.

    The Public Video Archive
        The (off-campus) permanent archive of all of :term:`The
        Conference`'s videos.

    The WiFi Network
        The WiFi network (SSID), for attendees of :term:`The Camp` and
        :term:`The Conference`.
        Covering :term:`The Videoed Talk Rooms`, :term:`The Hack Labs`,
        :term:`The BoF Rooms`, attendee accommodation, dining areas,
        :term:`The Front Desk`, and public spaces used by attendees.
        (FIXME locations).

    The Attendee Wired Network
        The VLAN / Network, for attendees of :term:`The Camp` and
        :term:`The Conference`, in hacklabs.

    The Video Network
        The dedicated VLAN / Network, for :term:`The Videography
        Equipment` to communicate with :term:`The Local Video Storage
        Server` and :term:`The Video Streaming Backend Server`.

    The Video Infrastructure
        The set of equipment consisting of :term:`The Local Video
        Storage Server`, :term:`The Video Distribution Network`,
        :term:`The Video Streaming Backend Server`, :term:`The Local
        Video Streaming Frontend Server`, :term:`The Video Transcoding
        Servers <The Video Transcoding Server>` and :term:`The
        Videography Equipment`.

    The Videography Equipment
        The equipment in :term:`The Videoed Talk Rooms` that will be
        used to record the presentations in HD video.
        Usually split into two islands in the room: a video team desk at
        the rear, and the podium in the front.

Network Requirements
--------------------

Communication
^^^^^^^^^^^^^

When working with existing networks, :term:`The Infrastructure Team` and
:term:`The Video Team` need a technical contact (a network admin) who
can be present during the conference.
Ideally on weekends and after hours, too.
We'll happily sponsor their attendance and food during the conference.

Attendees
^^^^^^^^^

IP Address Allocation and Firewall Configuration
""""""""""""""""""""""""""""""""""""""""""""""""

Attendees do not require public IP addresses.
Inbound connections from the Internet to Attendees may be filtered, per
venue network policies.
Outbound connections to :term:`The Internet` from attendees should not
be filtered.
Ideally, attendees will be offered both IPv4 and IPv6 connectivity.

A single VLAN can be used for both :term:`The WiFi Network` and
:term:`The Attendee Wired Network`.

:term:`The DHCP & DNS Server` can be used to SNAT all attendee network
traffic, or this could be done elsewhere if more convenient.

A ``/24`` IPv4 block is not big enough for the attendee networks, but a
``/22`` should be sufficient.

Wireless
""""""""

Ideally we'd provide Authenticated (PSK) access to :term:`The WiFi Network`
permitting attendees machines to reach both the :term:`The Internet`,
:term:`The Local Video Streaming Frontend Server`, and :term:`The Local
Mirror Server`.
The SSID should be accessible from FIXME (The first organiser arrivals
to :term:`The Camp`) until FIXME (the departure day from :term:`The
Conference`).

It would also be acceptable, but less desirable, to provide Individual
generic access tokens for access to :term:`The WiFi Network` with said
tokens provided to the conference organisers by FIXME (well before the
start of :term:`The Camp`).
There would need to uncapped, and useable by multiple devices per
attendee, without needing frequent re-authentication.

Observations regarding :term:`The WiFi Network`:

* This is a high-tech community.
  We anticipate that attendees will have between two and three devices,
  each.
* We recommend having >= 4 dual-band APs in each :term:`Videoed Talk
  Room <The Videoed Talk Rooms>` and :term:`Hack Lab <The Hack Labs>`.
* Attendees won't just be surfing the web during talks, but will be
  writing and testing code, all over the venue.
  This means a fair amount of download traffic from :term:`The Local
  Mirror Server` and :term:`The Internet`.
* Attendees will be using unusual Internet protocols, including a lot of
  ``ssh``, ``IRC``, and ``UDP``.

Wired
"""""

To foster collaboration on specific projects, rooms have been assigned
to serve as :term:`The Hack Labs`.

While most attendees will use :term:`The WiFi Network` in :term:`The
Hack Labs`, wired connectivity is useful for some embedded development
work, and large downloads.

There should be Unauthenticated access to :term:`The Attendee Wired
Network` permitting attendees machines to connect with each other (no
internal filtering), to connect to :term:`The Local Video Streaming
Frontend Server`, and to :term:`The Internet` (outbound only, if
necessary).

The venue or :term:`The Infrastructure Team` can provide local switches
(FIXME?) in each room but require two wired ports, with BPDU guard
turned off.
It is anticipated that there will be many attendees wishing to work from
:term:`The Hack Labs` so sufficient IP addresses must be made available.

Infrastructure
^^^^^^^^^^^^^^

IP Address Allocation and Firewall Configuration
""""""""""""""""""""""""""""""""""""""""""""""""

Ideally, the servers will be offered both IPv4 and IPv6 connectivity.

The following machines should have Publicly accessible IP addresses:

* :term:`The Local Mirror Server`
* :term:`The Local Video Storage Server`
* :term:`The Video Streaming Backend Server`

All of the conference's servers hosted in :term:`The Server Room` should
have un-firewalled access from :term:`The WiFi Network` and :term:`The
Attendee Wired Network`.

The following TCP ports should be accessible from :term:`The Internet`:

* :term:`The Local Video Storage Server` port 22 (ssh)
* :term:`The Video Streaming Backend Server` port 22 (ssh)
* :term:`The DHCP & DNS Server` port 22 (ssh)
* :term:`The Local Video Storage Server` port 80 (http) and 443 (https)
* :term:`The Video Streaming Backend Server` port 80 (http) and 443 (https)
* :term:`The Local Mirror Server` port 80 (http)

Some additional firewall changes may need to be made during the event.

The Video Network
"""""""""""""""""

Presentations delivered in :term:`The Videoed Talk Rooms` will be
videoed in HD and transcoded to a number of streaming formats to provide
local and non-local viewers with live video streams.

High throughput / low latency / QoS-enabled wired connections are
required between :term:`The Videography Equipment` (located in
:term:`The Videoed Talk Rooms`) and :term:`The Local Video Storage
Server`, :term:`The Video Streaming Backend Server`, :term:`The Local
Video Streaming Frontend Server`, and :term:`The Video Transcoding
Server` (located in :term:`The Server Room`).

If these connections are between buildings, there must be sufficient
bandwidth (1 Gbps for each of :term:`The Videoed Talk Rooms`) between
:term:`The Videoed Talk Rooms` and :term:`The Server Room` for the video
streams and copying recordings.

Within :term:`The Videoed Talk Rooms`, there must be gigabit
connectivity between the front and back of the room (:term:`The
Videography Equipment` islands).

This can be provided by running a cable within the room, and having a
gigabit switch connected to the room's uplink port (which would then
need no BDPU).

Ideally :term:`The Local Video Storage Server` would have a 10Gbps
connection to the core network.

Uplink capacity
---------------

Sufficient: 1Gbps.

Ideal: more.

Example Ideal setup
-------------------

What has worked well for us in the past is to have 3 VLANs:

#. Public IPs for servers (something like a ``/28``).
#. Attendee LAN (WiFi and wired), on an :rfc:`1819` ``/16``
#. Video LAN. :rfc:`1819` ``/24``

:term:`The DHCP & DNS server` can sit on all 3 networks and allocate
addresses.

Either the venue can provide a gateway that does routing between
networks (and NAT for Attendee and video).
Or given a multi-gigabit link to :term:`The DHCP & DNS server`, we can
handle this.

Optional Requirements
---------------------

The DHCP & DNS Server
    We have successfully used DHCP and DNS service provided by the
    venue, in the past, for attendee wired and wireless internet.
    :term:`The Video Network` needs its own DHCP and DNS, though.

The Local Mirror Server
    Given sufficient connectivity (or another on-campus mirror), we
    don't need to host our own mirror.

The Video Transcoding Server
    Given sufficient CPU capacity on the storage server, transcoding
    servers may not be needed.
    But they are usually desirable.

The WiFi Network
    Given a suitable guest-friendly network, we don't need our own SSID.
    However, this network should not have any ports firewalled, and
    ideally not require any registration.

The Attendee Wired Network
    Similarly, a dedicated wired network may not be necessary, given the
    right policies on an existing wired network.
