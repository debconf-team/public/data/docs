Budgeting
=========

DebConf is typically the main source of fund-raising and the largest
expenditures for Debian, in any year.
Each year the DebConf team prepares a budget for their event, estimating
the sponsorship that can be raised and expenses that will be incurred.

The DebConf Committee assists in preparing and reviewing the budget,
which is then presented to the DPL for approval.
Once the budget is approved, the DebConf team can make expenses within
the budget, without needing the DPL to approve items separately.

Usually, the first budget is relatively conservative, and as
sponsorship is raised the budget can be expanded (and re-approved by the
DPL) to provide bursaries for more attendees, and pay for some optional,
but nice to have conference expenses, like day trips.

Drafting the budget
-------------------

For the early stages of drafting a budget, a spreadsheet can be useful.
This allows experimenting with costs and attendee numbers.
Example from `DebConf 20 planning for Haifa <https://salsa.debian.org/debconf-team/public/data/dc20-dc21-haifa/-/blob/master/budget/budget.fods>`_.
(``.fods`` files are the git-friendly equivalent of ``.ods``)

Estimations from previous years
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Look at previous years' budgets and accounts to get a rough idea of
expenses.

The final budget lives in ``budget/budget.ledger`` in the conference
data repository.
You can find all of the DebConf data repositories
`on Salsa <https://salsa.debian.org/debconf-team/public/data>`_.

You'll need to estimate the number of attendees, average travel
bursary cost, accommodation costs, food costs, etc.

To guide you, every DebConf since 2019 has a published statistics page
on the conference website, with information from the registration
database:

* `DebConf 22 (Kosovo) <https://debconf22.debconf.org/statistics/>`_
* `DebConf 21 (online) <https://debconf21.debconf.org/statistics/>`_
* `DebConf 20 (online) <https://debconf20.debconf.org/statistics/>`_
* `DebConf 19 (Brazil) <https://debconf19.debconf.org/statistics/>`_

Older DebConfs have dedicated statistics pages on the old DebConf wiki:

* `DebConf 18 (Taiwan) <https://wiki.debconf.org/wiki/DebConf18/Statistics>`_
* `DebConf 17 (Taiwan) <https://wiki.debconf.org/wiki/DebConf17/Statistics>`_
* `DebConf 16 (South Africa) <https://wiki.debconf.org/wiki/DebConf16/FinalReport>`_

Even older DebConfs produced written final reports, containing statistics:

* `DebConf 6-17 Report Source <https://salsa.debian.org/debconf-team/public/data/reports>`_
* `DebConf 5-15 Published Report PDFs <https://salsa.debian.org/debconf-team/public/websites/media>`_

The conference closing presentation often contains event statistics.
Look for these in the `DebConf video archive
<https://debconf-video-team.pages.debian.net/videoplayer/>`_.

The DebConf website admins also have access to more detailed
information, and can run queries on previous event databases, if
necessary.

Considerations
~~~~~~~~~~~~~~

A balanced budget is always nice to aim for, but the budget doesn't
*need* to balance income with expenses.
DebConf usually runs at a profit for Debian, but the DPL is often
willing to cover the costs of holding an expensive DebConf in a more
expensive location.

We expect income from:

#. Sponsors. This is where the vast majority of income comes from.
#. Registration fees. Optional, but encouraged for corporate attendees.
#. Meals for self-paying attendees.
#. Self-paying accommodation (when organized through us).
#. Any drinks and snacks sold at the conference bar (when there is one).

When we're selling food or accommodation we usually include a moderate
markup, but still end up being very affordable (and usually the cheapest
option available for our attendees).
Our aim is to provide the most convenient options that serve the
attendees, while not losing any money.

Our expenses are:

#. Travel bursaries. This is usually the largest expense in the budget.
#. Travel for core conference organizers (including next year's team,
   and the core video team).
   We exclude this from the bursary budget, to make the spending
   clearer.
#. Accommodation. The expense here varies wildly, depending on whether
   it's camp beds in a classroom or 3-star hotel rooms. Generally we're
   aiming for university dormitory class accommodation.
#. Food. Simple, but varied and filling. We try to feed the attendees
   well.
#. Conference Venue. Typically this is provided for low / zero cost, by
   an institution.
#. Social events. The cheese & wine party, and the conference dinner.
#. Day Trips are typically self-paid by attendees, but when there is a
   budget surplus, the conference can cover them.
#. Video equipment. Typically the video team ships in some equipment,
   and hires other equipment, locally.
#. Swag for attendees. Typically a t-shirt, a bags, and a drinking mug.
#. Advertising material.
#. Local team costs during planning. Site visits, meals, etc.
#. Bank & fiscal sponsor fees.
#. Shipping thank you gifts to sponsors (swag).
#. Any drinks and snacks sold at the conference bar.

The ledger budget
~~~~~~~~~~~~~~~~~

Once you have estimates for attendee numbers, accommodation, food, and
the other major expenses, you should draft a simple text budget in
ledger format.

Create accounts for all the major expenses and income, as described
above.
Feel free to copy liberally from previous years.

Create a single entry in ``budget.ledger`` with all of the expected
income and expenses.
Use comments, to describe the items and how they were calculated.
Where possible, use calculated values including the per-person /
per-meal amount.

Record all the income/deficit against the primary trusted organization
for the event.

Approving the budget
--------------------

Once the Debconf Committee has reviewed the budget, it can be submitted
to the DPL for approval.

Send the ``budget.ledger`` and the converted calculated balance
(``./wrapper -f budget.ledger bal -X USD``) to the DPL for review, in an
email with ``debconf-team@lists.debian.org`` CCed.

When the budget is approved, tag the git repo, for future reference.

Adjusting the budget
--------------------

During the lead up to the event, we may need to adjust the budget a
couple of times.
This is often done to increase the budget for travel bursaries, once
significant sponsorship has been raised.

To prepare for these adjustments:

#. When major expense changes significantly, update the line for it in
   the budget.
#. As sponsorship is raised, update the projected sponsorship income.
   Increase it if we exceed expectations.
   Decrease it if the expectations are starting to look unrealistic.

When there are enough changes piled up, or we are about to commit to
spending outside the previously approved budget, re-submit it to the DPL
with a description of the changes.
