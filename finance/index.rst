****************
DebConf Finances
****************

.. toctree::
   :maxdepth: 2

   budgeting
   organizations
   accounting
