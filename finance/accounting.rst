.. _accounting:

DebConf Accounting
==================

Every year DebConf produces a set of accounts for the event finances.

.. _accounting-redactions:

Transparency and Privacy
------------------------

As we're a volunteer project, dependant on sponsorship, we publish
public accounts for transparency.

To respect our attendees, sponsors, volunteers and contributors privacy,
we have to redact some details in our accounting.

Names, addresses, unrelated purchases, etc. can be redacted in receipts.

Attendee payments are recorded by invoice number and type of income in
the public accounts.
Attendees aren't named.

Attendee bursaries aren't all recorded in the public accounts, only the
reimbursement totals.

Sponsor information is confidential and recorded in a separate
repository.
Publicly, in DebConf accounting, we just list each sponsorship by their
level in the ledger.
We reference the TO's or local organization's invoice in a comment,
leaving out the sponsor's name.

Reviewing the Accounts
----------------------

The accounts are in `ledger <https://www.ledger-cli.org/>`_ format.
We have a wrapper script around ledger that includes some useful default
options.

Summary of the budget in USD::

    $ ./wrapper -X USD -f budget.ledger bal

Summary of recorded income & expenses in USD::

    $ ./wrapper -X USD bal

Review all transactions in USD::

    $ ./wrapper -X USD reg --no-revalued --sort=date

Review all the expenses that SPI paid for in USD::

    $ ./wrapper -X USD reg assets:SPI -l 'any(account =~ /expenses:/)' --no-revalued -r --sort=date

Review all expenses paid by pollito in EUR::

    $ ./wrapper -X EUR reg liabilities:pollito -r --no-revalued --sort=date

Review all purchase from ACME::

    $ ./wrapper reg -l 'payee =~ /ACME/' --no-rounding

Export CSV of income received by SPI in USD::

    $ ./wrapper -X USD csv assets:SPI -l 'any(account =~ /income:/)' --no-revalued -r --sort=date

Currency
--------

DebConf accounts are usually multi-currency.
Our ledger setup supports this, record every entry in the appropriate
currency.
The ledger wrapper will download historic exchange rates and update
``forex.db`` to provide conversions to all supported currencies on every
transaction date.

To force an update of ``forex.db``, make it older than 1 day, and re-run
``wrapper``::

    $ touch --reference=README.md forex.db
    $ ./wrapper -X USD bal
    Updating currency exchange rates
    ...
    $ git commit -m "Update exchange rates" forex.db

Expenses
--------

When team members make expenses for the conference, they must have the
approval of the DebConf Treasurer in advance.
Incidental small expenditures are exempt up to a limit set by the
Treasurer.
The Treasurer will verify that the spending is in line with the budget.

Every expense should have an invoice/receipt (which may have :ref:`some
details redacted <accounting-redactions>`).
When this isn't possible, the team member can write a statement
attesting to the expense.
These should be emailed to the accounting team
(``accounting@debconf.org``), for recording in the books.

Expenses are recorded in ``expenses.ledger``, by payment date, with a
short description and a comment referencing the invoice/receipt.
The expense can be spread over multiple lines, if necessary, but usually
there is no reason to break it down.

The DebConf Treasurer will review the receipts and decide which expenses
can be claimed from the conference.

Large expenses are usually paid directly by a Trusted Organization, and
recorded against their asset account.

Small expenses are usually paid by team members, and recorded against a
liability account in their name.
At the end of the event (or periodically, if necessary) the team member
must be reimbursed by a Trusted Organization or a local organization.

Examples::

    2022-07-07 * Printer Hire - Rikon
        ; invoices/front-desk/20220707-printer-hire.pdf
        expenses:swag                                           250.00 EUR
        assets:FLOSSK

    2022-07-08 * Front Desk Phone SIM Card & Top-up - IPKO
        ; invoices/front-desk/20220708-fd-phone-sim.pdf
        expenses:incidentals                                      5.00 EUR
        liabilities:enkelena


Sponsorship
-----------

Income is recorded in ``income.ledger``, as negative values.

Once a sponsor has been invoiced by a Trusted Organization, it should be
recorded as income in the accounts, on the invoice date.
The name :ref:`is ommitted <accounting-redactions>`.
If the Trusted Organization charges a fee for collecting sponsorship
(e.g. SPI) this can be accounted directly in the income transaction,
against the fee account.

Example::

    2022-07-20 * Gold Sponsor
        ; spi/invoice_5688
        income:sponsors:gold                   -10.500,00 USD
        expenses:fees                              525,00 USD
        assets:SPI

Mark the transaction as pending (``!``) until the payment has been
settled, then as cleared (``*``).

Attendee Registration costs
---------------------------

Attendee registration costs are paid by card, online through Stripe.
We have scripts to export these payments into ledger format, that can be
used by team members who have access to the Stripe account::

    $ make clean all PYTHON=ve/bin/python
    rm -f stripe/payments.csv stripe/payouts.csv stripe/topups.csv
    ve/bin/python tools/export-stripe-data
    tools/stripe-payments-to-ledger > stripe-payments.ledger
    tools/stripe-payouts-to-ledger > stripe-payouts.ledger
    tools/stripe-topups-to-ledger > stripe-topups.ledger

Cash Payments
-------------

When the Front Desk takes a cash payment, they should issue an invoice
to the attendee, and a receipt for the cash payment.
These must then be manually added to ``income.ledger``.

Reimbursements
--------------

When a team member is reimbursed, the reimbursement should be recorded
in ``reimbursements.ledger``, with the date of the transaction, and any
appropriate RT ticket numbers.
If there are fees or a currency conversion involved, this can be
recorded, if known.
For example, here we know what was paid and what was received, the
remainder is accounted as fees::

    2022-09-13 * Reimbursement: Stefano
        ; Debian RT #8950
        ; SPI #5819
        liabilities:stefanor                                    147.00 EUR
        expenses:fees
        assets:SPI                                             -156.91 USD

Travel Bursaries
----------------

In coordination with the bursaries team, a list of attendees and their
authorized travel reimbursement amounts will be built for each Trusted
Organization that is involved in travel reimbursement.

Once these have been claimed, only the total amount claimed through each
Trusted Organization will be reported in the DebConf
``expenses.ledger``.

Closing the Books
-----------------

As the conference winds down, the DebConf accounting team will begin
work to close the books.
Typically the books are finally closed during the next DebCamp.

The procedure is:

#. Ensure on-site cash is accounted for and deposited.
#. Ensure all attendees have paid their attendance fees.
#. Ensure all suppliers have been paid.
#. Start the bursary reimbursement process.
#. Set a deadline for team members to submit all outstanding expense receipts.
#. Reimburse team members.
#. Set a deadline for travel bursaries to be claimed.
#. Ensure all sponsors have been invoiced, and recorded in the accounts.
#. Double-check with the TOs that all sponsor payments have been
   received. Update ledger record status to ``*``.
#. Ensure that local organizations that held assets for the event have
   been fully reimbursed and have returned any remaining balance.
#. Ask each involved Trusted Organization and local organization for a
   report of DebConf transactions for the year.
   Reconcile these against DebConf's accounts.
#. Close the books.
#. Report the final total income / cost of the conference to the DPL,
   The Debian Treasurers and DebConf team.
