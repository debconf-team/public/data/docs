Organizational Structure
========================

Where possible DebConf takes advantage of Debian's existing `Trusted
Organization (TOs)
<https://wiki.debian.org/Teams/Treasurer/Organizations>`_ to hold
assets, make and receive payments for the event.

Sometimes it is necessary to create a local organization, or find an
existing local organization to handle finances for the conference
within the venue country.
It typically isn't worth the effort (and takes too long) to spin up a
new TO for a single DebConf.

The DebConf Treasurer, in consultation with the Debian Treasurers and
DPL will determine which TOs and local organizations will be used for
the event, and in what capacities.
The DPL will need to sign-off on the use of a local organization that
isn't a TO, for DebConf.
This local organization can then contract with a TO to provide services
for the event.

Typically, the local organization makes smaller expenses and
reimbursements.
Large payments are made directly by a TO or transferred to the local
organization just in time to pay the vendor.
This reduces the risk of a balance remaining in a local organization.
