Organization Timeline
=====================

This is a rough timeline.
Take a copy and adjust it as appropriate for your schedule and task
slippage.
This schedule assumes that things will take longer than planned and
tries to get you to look at things early.
If you start things on this schedule, you can avoid a crunch in the
months before DebConf.

2 years before DebConf
----------------------

Decide to bid to host DebConf.
DebConf usually has a session where teams can announce their intention
to bid, give a 5-minute presentation of their city, and take Q&A.

During the next months, form a local team and start writing a formal
bid to host DebConf. (FIXME LINK)
Read previous bids, budgets, and reports to see what works well and what
failed in the past.

Join the debconf-team mailing list (see :ref:`mailing-lists`), the
#debconf-team IRC channel (FIXME LINK), and follow the team meetings.
Get to know the team and processes.

Create a git repo on salsa, an IRC channel on OFTC, and a mailing list
on lists.debian.org, to coordinate your bid, if desired.
Ideally, recruit a past DebConf organiser to join your team as an
adviser.

22 months before DebConf
------------------------

Submit the written bid. (FIXME LINK)

Make provisional bookings of your preferred venues, if possible.

20 months before DebConf
------------------------

Your bid bid is selected to host DebConf, congratulations!

Or, unfortunately it isn't, and you continue to work on your bid for the
next year, making it even stronger.

You can now confirm your venue booking, and start to investigate
suppliers for accommodation, catering, swag, child-care, etc.
Start thinking about design, the conference will need a logo,
colour-scheme, a font, website, and eventually t-shirts and name-tags.
You have lots of time, but the more work you can do in advance, the
easier things will be in the weeks before DebConf.

If you need to make payments at this point, you can ask for approval
from the DPL.
Once you have a DPL-approved budget, you'll have your own spending
authority, so start thinking about your conference budget.
If you're going to need to create a local company to hold assets and/or
sign contacts for the conference, start the process ASAP. (FIXME LINK)

This a good time to look at the at the organizational roles
(see :ref:`team-roles`) and get your local team to join as many of these
as possible.
Being organizers for the next DebConf will help them to get bursaries to
attend it, and prepare them for your upcoming DebConf.
If your bid wasn't selected, this is a great way to strengthen your bid
for next time.

Build your local team, and get as many of them to DebConf as possible.
Hold in-person meetings of your local team, if you can, to build local
motivation.
Also encourage the team to attend the DebConf Team IRC meetings and get
to know the wider global team.

Start to build lists of local companies to approach for sponsorship, and
find the best contacts to reach them through.
In coordination with the DebConf sponsors team, reach out to as many as
possible, to find out their deadlines for sponsorship decisions (budgets
are often set years in advance).
We fundraise for the upcoming DebConf, not the one after that, so when
you reach out be mindful not to poach sponsors from the preceding
DebConf.

18 months before DebConf
------------------------

Now that we've confirmed the venues, schedule meetings with their
representatives to figure out requirements.

Identify rooms within the venues for talk tracks, adhoc BoFs, hack-labs,
meals, front desk, organiser's office, video team NoC, and social
spaces. (FIXME LINK)

Determine what insurance the venues have for accidents and theft at the
conference, and what, if any, insurance we will need to purchase. (FIXME
LINK)

Meet the venue caterers and see if they are the best option, or we
should bring in external caterers.
Find out what facilities will be available to external caterers, and
at what cost.
Find licensing requirements for serving alcohol in hack-labs and the
Cheese and Wine party, this may dictate their location.

Meet with the network team to get commitments for sufficient WiFi
coverage across the necessary venues. (FIXME LINK)
Organize access to a server room or a make-shift server room in a
lockable office with sufficient connectivity and cooling.
Budget for extra equipment and ISPs, if necessary.

In consultation with the DebConf Video team, meet with the A/V team to
get commitments on access to A/V equipment, and spaces in the venues.
Budget for extra equipment hire, if necessary.

As well as financial sponsors, start looking for local infrastructure
sponsors for network gear, on-site servers and computers for the front
desk and video team. (FIXME LINK)
Local hosting providers often have equipment to spare.

1 year before DebConf
---------------------

Attend DebConf, helping out with the organization as much as possible.
Spend time with the previous team to see the issues they encounter and
learn from them.

Put out a call for DebConf+2 bids to present their proposals.

Give a presentation at DebConf, showcasing your city, and giving
attendees an idea of what to expect next year.
The conference dates should be finalized by this point, and can be
announced.
Expect lots of questions from attendees after the presentation and
throughout DebConf.

The previous DebConf team is probably exhausted after running DebConf,
and things will move slowly for the next few months.
You'll need to keep driving things to prepare for your DebConf.
If the previous meeting schedule wasn't well suited to your time-zone,
poll the team and find a time-slot that works for as many people as
possible (but primarily one that works for your local team).

Put out a call for feedback after DebConf.
Read the feedback from DebConf attendees, so you can continue to make
DebConf better.

If you don't have a logo designed yet, you'll need one soon.

It's now time to launch the website for your next DebConf, and start
fundraising.
Produce a sponsorship brochure, based on previous years, and decide on
appropriate sponsorship packages to offer.
Contact the local sponsors you have previously identified, and send them
the brochure and flyer.
Contact previous DebConf sponsors, from the DebConf Sponsorship Team's
database of contacts.

You'll need to be able to spend money, soon, so finalize a budget and
submit it to the DPL for approval.
Contact all of the DebConf teams and ask for their budget needs.
Run your proposed budget past the DebConf Committee and the DPL for
approval.

10 months before DebConf
------------------------

Set a deadline for DebConf+2 bids.

Keep raising sponsorship.

8 months before DebConf
-----------------------

Decide on the hosts for DebConf+2.

Figure out the important dates for the conference, and write them up in
an "important dates" web page:

#. Call for Proposals.
#. Registration opening.
#. Bursary submission deadline.
#. Bursary decision deadline.
#. Final date for submitting events for the official schedule.
#. Final date to accept / withdraw a travel bursary, for the first round
   of recipients.
   Time it so that additional rounds can be offered, and bursary
   recipients will have enough time to get a visa, if necessary.
#. Final date to accept / withdraw a food/accommodation bursary, so that
   accommodation numbers can be finalized.
#. Final date to register for conference-provided accommodation (if
   necessary).
#. Final date to register with guaranteed swag. We generally allow
   on-site registration during the event, up to venue safety limits, but
   don't provide guaranteed swag for late registrants.

You'll also need to work with your venues and caterers to find deadlines
to commit to attendee numbers and mechanisms to allow last-minute
adjustments.

Select a t-shirt supplier, and get a size-chart, so we can offer the
correct sizes to attendees during registration.

Prepare to open registration and talk submission early in the year.
Design the name-tags so that attendees can preview them on registration.
Test the registration form on the website, and make any necessary
changes to it.

Form a content team and write a Call for Proposals.
Form a list of speakers you want to invite to the conference, local and
international.

Form a visa team to gather information about travel requirements and
restrictions, documentation requirements and visa timelines.
Document these on the website.
If invitation letters can be helpful for visa applicants, find a
friendly authority to issue them.

Keep raising sponsorship.

7 months before DebConf
-----------------------

Once businesses start re-opening in the new year, start contacting them
to raise sponsorship.

If you haven't, yet, start to select suppliers for catering, child-care,
swag, etc. and contract them, as appropriate.

Issue the Call for Proposals.

Issue invitations to any invited speakers.

6 months before DebConf
-----------------------

Start to review talk submissions and give feedback to submitters if
necessary.

Open registration.

Call for a bursaries team to review submissions.

Keep raising sponsorship.

4 months before DebConf
-----------------------

Close bursary applications and start making the first round of bursary
decisions.

Publish a preliminary conference schedule.
Solicit feedback from presenters, and adjust as necessary.

Issue invitation letters to attendees that need them and have received
bursaries or are self-funded and seem legitimate.

Keep raising sponsorship.

3 months before DebConf
-----------------------

If sponsors want to ship materials or swag, get them to do it soon, to
avoid being stuck in customs during the conference.

Keep raising sponsorship.

2 months before DebConf
-----------------------

Finalize attendee numbers for accommodation.

Finalize child-care numbers, and confirm or cancel it, based on demand.

If there is predicted to be a big budget surplus or short-fall, there is
an opportunity to adjust spending at around this point.
You can increase or decrease the budget for nice-to-have parts of the
conference.
Or issue a final round of bursaries if there are still strong candidates
who haven't been able to be funded, yet.

Produce arrival instructions for attendees, with maps and front-desk
contact details.

Prepare day-trip options, and put out a call for sign-up.
Book buses for transport, if necessary.

Confirm caterers, venues, network, AV equipment, etc.

Finalize swag quantities and get them manufactured.

Do a rough calculation of t-shirt quantities by size, based on
registration statistics and previous years.
Figure out numbers for the Video Team and Orga team shirts.
Make some extras for sponsors, and attendees to change sizes.

Keep raising sponsorship, this is the last chance for sponsors to get
their logo on t-shirts and banners.

1 month before DebConf
----------------------

Finalize t-shirt designs and numbers, and send them to the printers.
Manufacture banners for the venues.

Finalize the welcome email and arrival instructions.

2 weeks before start of DebCamp
-------------------------------

Send welcome email with arrival instructions to attendees. Send it
early, because some attendees will travel before arriving at the
conference.

DebCamp & DebConf
-----------------

Host an amazing DebConf!
You won't see much of it, because you'll be busy making it happen.

1 month after DebConf
---------------------

Put out a call for feedback, so you can see what you got right and what
didn't work.
The next year's team can learn from this.

Send thank you notes, t-shirts, and other swag to sponsors.
