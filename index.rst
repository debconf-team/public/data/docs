DebConf Organiser's Guide
=========================

Welcome to the DebConf Team documentation.

.. attention::

    This documentation is still a work in progress.

    Some of this documentation may consist mainly of links to other
    sources, other parts may need to be written from scratch.

    The most important thing is that there is a single URL that we can
    point people to that references all the documentation, rather than
    having to find bits and pieces spread across various websites and
    systems.

This document is meant to be an introduction to DebConf organization for
new organisers and a reference for team members.

The organization evolves as the project and conference evolves, please
file issues and merge requests to update this documentation if you find
missing information and/or inaccuracies.

.. _index:

Index
=====

.. toctree::
   :maxdepth: 2

   overview
   mailing_lists
   team_roles
   timeline
   network_requirements
   finance/index
