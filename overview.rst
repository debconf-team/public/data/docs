DebConf Overview
================

DebConf is the annual conference for `Debian <https://www.debian.org/>`_
contributors and users interested in improving Debian.
Debian is a Linux distribution, an operating system for computers,
composed of free and open-source software.

The first DebConf was held in 2000, bringing together Debian
contributors from all around the world.
Over time, the conference has grown into an event of around 500
in-person attendees.
The content live-streamed over the Internet to remote participants.
Every year the conference is held in a different country, organised by a
new team.
Teams bid to host the conference in their city two years in advance.

The event typically runs for two weeks.
The main conference, DebConf, takes place in the second week with scheduled
talks and meetings.
There is one day allocated in the middle of DebConf for day trips and
excursions.
The first week, known as DebCamp, does not have a formal schedule and
provides space for Debian contributors to work, individually as well as
in groups, on Debian projects.
Attendees usually arrive over the course of DebCamp, with numbers slowly
growing from ~10 on day 1 to ~200 by the end of DebCamp.
Conference build-up happens in parallel to DebCamp, starting a day or
two before it.

The attendees are mostly software engineers from a wide range of
backgrounds, including corporate executives, hobbyists, students, and
academics.
The atmosphere is rather relaxed and informal, with attendees
socialising together, but also working productively on their laptops.

Conference Content
------------------

DebConf typically has 3 tracks of scheduled talks with recorded and
streamed video.
One of them will be in a large auditorium that can seat most of the
conference attendees, for plenary sessions and popular events.
The other two can be smaller venues (seating around 200) for smaller
events.

Most events are typical academic-style conference presentations, with
Q&A from the audience at the end.
The other common type of event is a Birds of a Feather (BoF) session,
which is a group meeting or open discussion (possibly introduced by a
short presentation).
There are also typically some panel discussions and Q&A sessions,
lightning talks, a poetry event, and sometimes live music.

Most content is in English, the common language of Debian Development,
but there are often talks in the local language, aimed at local
attendees, who may be new to Debian.

In parallel to the main auditoria there are a few smaller rooms for
ad-hoc presentations and BoFs, without video coverage.
These are available throughout DebCamp and DebConf.

There are a few "hack-lab" work-spaces for attendees to work on their
laptops, individually and in very small groups.
These are the focus of DebCamp, and are set up at the start of it.
Typically we have one noisy hack-lab with a bar for sociable hacking,
and one quiet hack-lab for more focused work.
When possible we also have an outdoor hack-lab.

Social Events
-------------

The usual scheduled social events are the cheese and wine party and the
official conference dinner.

Attendees bring cheese and wine (and other tasty things) from their home
countries, when permitted by Customs, to share at the party.
It's usually held on an evening early in DebConf.

The conference dinner is usually held off-site towards the end of
DebConf.

As many of the conference attendees are colleagues who only see each
other once a year, the conference has a strong social component.
Groups will work together in the hack-labs and gardens, go out to dinner
together, and socialise in the pub during the conference and after
hours.

We usually have a hack-lab with a bar, to create an affordable on-site
social space in the evenings and allow the attendees to be productive
while socialising.

Travel Bursaries
----------------

The conference provides travel, accommodation, and food bursaries.
This is usually the largest part of the conference's budget.

Accommodation
-------------

For attendees with conference-sponsored accommodation, we typically
provide low-cost shared rooms.
These are university dormitory rooms when available, but have ranged in
the past from camp-beds in school classrooms to private hotel rooms, as
available.

Self-paid attendees will often want to use the conference-organised
accommodation, when possible, for social reasons and convenience.

Catering
--------

The conference typically provides 3 meals a day for attendees.
The food is usually fairly basic, and locally-inspired.

Many of the attendees will have their meals sponsored, but self-paid
attendees can also purchase meal vouchers.

There are a wide range of diets to cater for: many vegetarians and
vegans, some religious dietary restrictions, and a range of allergies.
Typically, we can't cater to all the allergies and special needs, but
try to make a best effort.

Accessibility and Diversity
---------------------------

The DebConf audience is large and varied.
We accommodate attendees with reduced mobility and in wheelchairs, as
well as with sight and/or hearing impairments.
There is also a visible LGBTQ presence among our attendees.

Our attendees come from a wide range countries:
Some come from countries not recognised by the host country, some are
stateless, and some live in countries under economic sanctions.

Budget
------

Typically the conference runs a budget of around USD 200,000, including
travel bursaries.
While this is usually more than covered by sponsorship raised for the
event, many sponsors don't commit until late in the process, and Debian
has to commit to covering the budget before the funds are raised.

Attendee numbers and sponsorship can be hard to predict until weeks
before the event, so we try to maintain some flexibility in
accommodation and catering quantities.

Infrastructure
--------------

Internet access is essential for attendees to work on Debian: in
auditoria, meeting rooms, hack-labs, dormitories, dining rooms, smoking
areas, gardens, and bars.
The conference usually provides free high-speed, unfiltered WiFi
connectivity in all of these spaces, with the help of the venues.
