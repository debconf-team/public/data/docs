.. _team-roles:

==========
Team Roles
==========

Running a DebConf is a big enough project that it requires a team.
While many DebConfs end up with one or two organisers doing most of the
work, this just leads to burnout.

Many tasks in DebConf have long-running teams that help out every year.
The local team should usually appoint at least one team member to join
each team and lead the more inactive ones.

The roles your local team should fill are described below.

Chief organiser
---------------

The chief organiser is in charge of everything.
This role entails being first point of contact for everyone and
representing the conference to the outside world.
This is a very communicative and intermittently extremely time-consuming
role.
This can of course also be taken on by a team which works together
extremely closely.

Your team should appoint at least 1 chief organiser, with some
assistants.

Artwork
-------

Finalises artwork, co-ordinates with community or paid-for artists.

`Debian Design Team <https://wiki.debian.org/Teams/Design>`_:
`design-devel@alioth-lists.debian.net
<https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/design-devel>`_.
DebConf also has some artists in its community.

Your team should appoint at least 1 coordinator, to work with the
community or a paid design consultant.

Bursaries
---------

Decide who gets sponsored travel and accommodation for the conference.
This team usually consists of long-time DebConf attendees who are not
applying for bursaries themselves.

DebConf has a standard process for reviewing bursaries, and some
long-term team members.
Bursaries Team: bursaries@debconf.org, #debconf-bursaries (private) on
IRC.

Your team should appoint at least 1 team member to help the bursaries
team to rate applicants.
Ideally someone who can speak the local languages and knows the local
Free Software projects and contributors that are new to DebConf.

Cheese & Wine
-------------

Collects cheese & wine donations from arriving DebConf attendees, and
prepares for the now-traditional DebConf Cheese & Wine party.


DebConf has a standing Cheese and Wine team, and generally appoints an
experienced DebConf attendee as Cheese Team Organizer at the previous
DebConf.

Your team should provide a liaison to the cheese team, to coordinate a
venue for the party, licensing (if necessary) and cheese storage during
the event.

Content
-------

Defines the content of the conference: keynotes, talks, schedule, etc.
Responsible for drafting a Call for Proposals, inviting submissions,
scoring submitted events, and building an event schedule.

During the event, talk meisters represent the content team in the room,
coordinating with the speakers, organising the audience questions, and
ending the session on time.

DebConf has a standard process for reviewing event submissions, and some
long-term team members.
Content Team: content@debconf.org, #debconf-content on IRC.

Your team should appoint at least 1 team member to coordinate the
conference content, and ideally several local volunteers to act as talk
meisters during the event.

Day Trip
--------

Organizes the day trip, and serves as point of contact on the day.

DebConf is a long event, and everyone needs a break half-way through it.
This is also an opportunity to showcase your area's attractions
(tourist destinations, natural beauty, and technically interesting).

Usually it makes sense to offer several alternative day-trip options for
sign-up, so that each one has a manageable group size.
Transport and destination costs can be covered by the conference or
attendees, directly, as appropriate.

This task is inherently local, so your team should handle this entirely.

Diversity and Welcoming newcomers
---------------------------------

Contact point for diversity inclusion and welcoming newcomers.

Your team should appoint at least 1 member to the team to help welcome
local newcomers.
Preferably someone who speaks the local languages.

Debian has two teams in this area:
`Debian Diversity Team <https://wiki.debian.org/Teams/Diversity>`_:
diversity@debian.org, #debian-diversity on IRC.
`Debian Welcome Team <https://wiki.debian.org/Teams/Welcome>`_:
#debian-welcome on IRC.

Facilities liaison
------------------

Remains in constant contact with the venue's management, and relays
queries, desires and alterations between the team and the venue staff.

The conference venue is critical to the event, and usually requires
negotiation and coordination up to and during the event.
The venue's management should have a single point of contact for issues
related to DebConf.

Your team should provide at least 1 coordinator for the venue.
This role can be broken up by venue / supplier as appropriate (e.g.
someone who is responsible for working with the caterers).

Fundraising
-----------

Determines the sponsorship levels and takes charge of the contacting
past sponsors and potential new sponsors.

DebConf has a standing sponsors team with established processes and a
database of historical sponsors.
Sponsors Team: sponsors@debconf.org, #debconf-sponsors on IRC.

Your team should provide at least 1 volunteer to join the sponsors team.
And the rest of the team should all use their personal networks to raise
funding from local and international companies.

Publicity
---------

Spreads the word about DebConf using the Debian Publicity channels, and
DebConf specific channels.
Supplies the local press with news items frequently and regularly, and
serves as their contact person on the phone and in person.

Debian has a standing publicity team, with established processes and
publication channels.
`Debian Publicity Team <https://wiki.debian.org/Teams/Publicity>`_:
`debian-publicity@lists.debian.org
<https://lists.debian.org/debian-publicity/>`_, #debian-publicity on
IRC.

Your team should have at least 1 member responsible for drafting press
releases for the conference, and leasing with the Debian publicity team.

Registration and Front Desk
---------------------------

Before the conference, general point of contact, during the conference,
the team that solves attendees' problems, or refers them to someone who
can.
The registration team also handles accommodation shared-room assignment,
and serves as the participants' point of contact for their
conference-organised accommodation.

The Front Desk is staffed during the conference, and is reachable
after-hours by cellphone.

DebConf has a standing registration team, with established processes.
Registration team: registration@debconf.org, #debconf-registration
(private) on IRC.

Your team should provide at least 1 member to join the registration
team.
Preferably someone who speaks the local languages and can help attendees
with local issues.

Treasurer
---------

Drafts the budget in cooperation with the team, ensures it is adhered to
or adjusted, maintains all accounts and is in charge of all monetary
transactions.

DebConf funds are held by `Debian Trusted Organizations
<https://wiki.debian.org/Teams/Treasurer/Organizations>`_.
Once a budget is approved, the DPL usually delegates authority for
spending this budget to the DebConf treasurer.

Your team should provide at least 1 member to act as the DebConf
treasurer.
Preferably this should be someone known in the Debian community who can
be delegated spending authority by the DPL.

DebConf has an :ref:`established process <accounting>` for managing its
books.

Video
-----

The team who does the live streaming of the conference, and generates
video recordings of the events.

DebConf has a standing video team, and some equipment that we ship
internationally to DebConf.
`DebConf Video Team <https://debconf-video-team.pages.debian.net/docs/>`_:
`debconf-video@lists.debian.org
<https://lists.debian.org/debconf-video/>`_, #debconf-video on IRC.

Your team should provide at least 1 member to act as a liaison to the
DebConf video team, the venue, and local AV equipment suppliers.
Preferably this person should have some experience in theatre / AV.

Your team should try to recruit as many local volunteers as possible to
help the video team during the conference.
The video team will manage setup/tear-down and provide training.

Visa
----

Helps attendees on visa issues and invitation letters.
Background check if attendee is a visa-seeker or a real attendee.
Coordinates with the registration team, venue and local political
contacts, as necessary.

Your team should provide at least 1 member to run the visa team.

Website
-------

Designs, implements, and maintains the website, including all content
which doesn't fall within the remit of other specific team members.

DebConf has a standing website team, with an established stack built
on top of `wafer <https://wafer.readthedocs.io/en/latest/>`_ over
several years of DebConf.

Your team should provide at least 1 member to work with the DebConf
website team and the design team to produce a functional and unique
DebConf website.
