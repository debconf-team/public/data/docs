.. _mailing-lists:

Mailing Lists
=============

E-mail is used for asynchronous and recorded discussions.
Those should typically be most of the discussions as people have varying
time zones and commitments.

We use several `mailing lists`_.
The `debconf-team`_ list is used for the general organization of the
conference.
You can post a message to each mailing list using its address.
You can also subscribe to any of the public lists to receive messages
posted there.
You do not need to be subscribed in order to post, but without
subscribing you may miss replies.

.. _mailing lists: https://lists.debian.org/debconf.html
.. _debconf-team: https://lists.debian.org/debconf-team/
